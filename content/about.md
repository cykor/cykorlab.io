---
title: "About"
comments: false
---

**Cykor** is cyber security club at [Korea University](https://korea.ac.kr/) consists of undergraduate students majoring in Cyber Defense.
	
### Contact
- <mailto:korea.cykor@gmail.com>
- Advisor: SeungJoo Kim (<mailto:seungjoo.kim@gmail.com>)

### News
- [\[인터뷰\] 일본 최대 규모의 해커대회 우승 거머쥔 사이버국방학과 CyKor](https://korea.ac.kr/user/boardList.do?boardId=135&id=university_060102000000&boardSeq=473320&command=albumView), Feb 2017
- [Interview with the CyKor CTF team](https://blog.avatao.com/Interview-CyKor/), Oct 2016

### Awards
- **CTFZone 2018**, 4th place, Moscow, Russia Nov 2018
- **Belluminar POC 2018**, 1st place, Seoul, Korea Nov 2018
- **CODE BLUE CTF 2018**, 1st place, Tokyo, Japan Nov 2018
- **Whitehat Grand Prix 2018**, 2nd place (CoconutCoffee), Hanoi, Vietnam Nov 2018
- **Google Capture The Flag 2018**, 6th place (DEFKOR00T), London, England Oct 2018
- **HITCON CTF 2018**, 8th place, Oct 2018
- **Hack.lu CTF 2018**, 5th place, Oct 2018
- **TokyoWesterns CTF 4th 2018**, 5th place Sep 2018
- **HITB-XCTF GSEC CTF**, 3rd place, Singapore, Aug 2018
- **DEFCON 26 CTF**, 1st place (DEFKOR00T), Las Vegas, USA, Aug 2018
- **Belluminar Beijing 2018** 3rd place, Beijing, China, Jul 2018
- **0CTF/TCTF 2018**, 2nd place, Shenzhen, China, May 2018
- **RCTF 2018**, 1st place, May 2018
- **DEFCON China/BCTF 2018**, 3rd place, May 2018
- **CODEGATE CTF 2018**, 1st place, Seoul, Korea, Apr 2018
- **SECCON CTF 2017**, 1st place, Tokyo, Japan, Jan 2018
- **HITCON CTF 2017**, 1st place (Cykorkinesis), Taipei, Taiwan, Dec 2017
- **Whitehat Contest**, 3rd place, Seoul, Korea, Nov 2017
- **Trend Micro CTF 2017**, 4th place, Tokyo, Japan, Nov 2017
- **Cyber Conflict Exercise 2017**, 1st place, Korea, Nov 2017
- **White Hacker League 2017**, 1st place, Korea, Nov 2017
- **CODE BLUE CTF 2017**, 2nd place, Tokyo, Japan, Nov 2017
- **Kaspersky Industrial CTF 2017**, 1st place, Shanghai, China, Oct 2017
- **XCTF 2017**, 2nd and 4th places (Motesolo and CyKor), Beijing, China, Sep 2017
- **DEFCON 25 CTF**, 4th place (DEFKOR), Las Vegas, USA, Jul 2017
- **SECUINSIDE CTF 2017**, 2nd place, Jul 2017
- **PlaidCTF 2016**, 2nd place (DEFKOR), Apr 2017
- **CODEGATE CTF 2017**, 2nd place, Seoul, Korea, Apr 2017 
- **SECCON CTF 2016**, 1st place, Tokyo, Japan, Jan 2017 
- **HITCON CTF 2016**, 1st place (Cykorkinesis), Taipei, Taiwan, Dec 2016
- **HDCON 2016**, 1st place, Seoul, Korea, Dec 2016
- **Trend Micro CTF 2016**, 5th place, Tokyo, Japan, Nov 2016
- **Whitehat Contest 2016**, 1st place, Seoul, Korea, Oct 2016
- **DEFCON 24 CTF**, 3rd place (DEFKOR), Las Vegas, USA, Aug 2016
- **XCTF 2016**, 1st place, Beijing, China, Jul 2016
- **Belluminar POC 2016**, 1st place, Seoul, Korea, Nov 2016
- **Belluminar Beijing 2016**, 2nd place, Beijing, China, Jun 2016
- **PlaidCTF 2016**, 2nd place (DEFKOR), Apr 2016
- **CODEGATE CTF 2016 (University)**, 1st place, Seoul, Korea, May 2016
- **SECCON CTF 2015 (International)**, 1st place (Cykorkinesis), Tokyo, Japan, Jan 2016
- **HITCON CTF 2015**, 1st place (Cykorkinesis), Taipei, Taiwan, Dec 2015
- **Trend Micro CTF Asia 2015**, 1st place, Tokyo, Japan, Nov 2015
- **Inc0gnito CTF 2015**, 1st place, Aug 2015
- **DEFCON 23 CTF**, 1st place (DEFKOR), Las Vegas, USA, Aug 2015
- **Codegate CTF 2015**, 4th place, Seoul, Korea, Aug 2015
